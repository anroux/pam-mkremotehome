#include <sys/types.h>
#include <sys/stat.h>
#include <sys/time.h>
#include <sys/resource.h>
#include <sys/wait.h>
#include <unistd.h>
#include <pwd.h>
#include <errno.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <syslog.h>

#include <curl/curl.h>
#include <json.h>

#include <security/pam_modules.h>
#include <security/_pam_macros.h>
#include <security/pam_modutil.h>
#include <security/pam_ext.h>

/* argument parsing */
#define MKHOMEDIR_DEBUG      020	/* be verbose about things */
#define MKHOMEDIR_QUIET      040	/* keep quiet about things */

struct options_t {
  int ctrl;
  const char *api_url;
  const char *skelname;
};
typedef struct options_t options_t;

static void
_pam_parse (const pam_handle_t *pamh, int flags, int argc, const char **argv,
	    options_t *opt)
{
   opt->ctrl = 0;
   opt->api_url = "https://unixhome-api.esrf.fr/webhook/NONEXISTENT";
   opt->skelname = "esrf";

   /* does the application require quiet? */
   if ((flags & PAM_SILENT) == PAM_SILENT)
      opt->ctrl |= MKHOMEDIR_QUIET;

   /* step through arguments */
   for (; argc-- > 0; ++argv)
   {
      if (!strcmp(*argv, "silent")) {
	 opt->ctrl |= MKHOMEDIR_QUIET;
      } else if (!strcmp(*argv, "debug")) {
         opt->ctrl |= MKHOMEDIR_DEBUG;
      } else if (!strncmp(*argv,"apiurl=",7)) {
	 opt->api_url = *argv+7;
      } else if (!strncmp(*argv,"skelname=",9)) {
	 opt->skelname = *argv+9;
      } else {
	 pam_syslog(pamh, LOG_ERR, "unknown option: %s", *argv);
      }
   }
}

/* Do the actual work of creating a home dir */
static int
create_homedir (pam_handle_t *pamh, options_t *opt,
		const char *user, const char *dir)
{
   int retval = 0;

   CURL *curl;
   CURLcode res;
   struct curl_slist *headers = NULL;

   struct json_object *jobj;
   const char *json_content;


   /* Mention what is happening, if the notification fails that is OK */
   if (!(opt->ctrl & MKHOMEDIR_QUIET))
      pam_info(pamh, "Creating home directory for %s on central storage. It can take a while, please be patient...", user);


   D(("called."));

   if (opt->ctrl & MKHOMEDIR_DEBUG) {
      pam_syslog(pamh, LOG_DEBUG, "Calling the remote home creation endpoint.");
   }

   curl = curl_easy_init();
   if (!curl) {
      pam_syslog(pamh, LOG_ERR, "Unable to initialize cURL.");
      return PAM_SYSTEM_ERR;
   }

   curl_easy_setopt(curl, CURLOPT_URL, opt->api_url);

   headers = curl_slist_append(headers, "Content-Type: application/json");
   curl_easy_setopt(curl, CURLOPT_HTTPHEADER, headers);

   jobj = json_object_new_object();
   json_object_object_add(jobj, "username", json_object_new_string(user));
   json_object_object_add(jobj, "skel", json_object_new_string(opt->skelname));

   json_content = json_object_to_json_string_ext(jobj, JSON_C_TO_STRING_PLAIN);
   curl_easy_setopt(curl, CURLOPT_POSTFIELDS, json_content);

   res = curl_easy_perform(curl);
   if (res != CURLE_OK) {
      const char *err_string;

      retval = PAM_SYSTEM_ERR;

      err_string = curl_easy_strerror(res);
      pam_syslog(pamh, LOG_ERR, "Unable to send the request: %s.", err_string);
   } else {
      long response_code;
      curl_easy_getinfo(curl, CURLINFO_RESPONSE_CODE, &response_code);

      if (opt->ctrl & MKHOMEDIR_DEBUG) {
         pam_syslog(pamh, LOG_DEBUG, "The API answered with HTTP/%ld", response_code);
      }

      if (response_code >= 200 && response_code < 300) {
         struct stat St;
         int ttl = 60;

         /* Assume the home does not exist until the check */
         retval = PAM_SYSTEM_ERR;

         if (opt->ctrl & MKHOMEDIR_DEBUG) {
            pam_syslog(pamh, LOG_DEBUG, "Waiting for the home to appear...");
         }
         while (ttl-- > 0) {
            if (stat(dir, &St) == 0) {
               retval = PAM_SUCCESS;
               break;
            }
            sleep(1);
         }

      } else if (response_code >= 400) {
         /*const char *err_string;
         struct json_object *parsed_json;
         struct json_object *err_text;*/

         pam_syslog(pamh, LOG_ERR, "An error occured during the home creation.");
         retval = PAM_SYSTEM_ERR;

         /*parsed_json = json_tokener_parse(err_string);
         json_object_object_get_ex(parsed_json, "error", &err_text);

         pam_syslog(pamh, LOG_ERR, json_object_get_string(err_text));*/
      }
   }

   json_object_put(jobj); // Delete the json object
   curl_slist_free_all(headers);
   curl_easy_cleanup(curl);

   if (retval != PAM_SUCCESS && !(opt->ctrl & MKHOMEDIR_QUIET)) {
      pam_error(pamh, "Unable to create and initialize directory '%s'.", dir);
   }

   D(("returning %d", retval));
   return retval;
}

int
pam_sm_open_session (pam_handle_t *pamh, int flags, int argc,
		     const char **argv)
{
   int retval;
   options_t opt;
   const void *user;
   const struct passwd *pwd;
   struct stat St;

   /* Parse the flag values */
   _pam_parse(pamh, flags, argc, argv, &opt);

   /* Determine the user name so we can get the home directory */
   retval = pam_get_item(pamh, PAM_USER, &user);
   if (retval != PAM_SUCCESS || user == NULL || *(const char *)user == '\0')
   {
      pam_syslog(pamh, LOG_NOTICE, "Cannot obtain the user name.");
      return PAM_USER_UNKNOWN;
   }

   /* Get the password entry */
   pwd = pam_modutil_getpwnam (pamh, user);
   if (pwd == NULL)
   {
      pam_syslog(pamh, LOG_NOTICE, "User unknown.");
      D(("couldn't identify user %s", user));
      return PAM_USER_UNKNOWN;
   }

   /* Stat the home directory, if something exists then we assume it is
      correct and return a success*/
   if (stat(pwd->pw_dir, &St) == 0) {
      if (opt.ctrl & MKHOMEDIR_DEBUG) {
          pam_syslog(pamh, LOG_DEBUG, "Home directory %s already exists.",
              pwd->pw_dir);
      }
      return PAM_SUCCESS;
   }

   return create_homedir(pamh, &opt, user, pwd->pw_dir);
}

/* Ignore */
int pam_sm_close_session (pam_handle_t * pamh, int flags, int argc, const char **argv)
{
   return PAM_SUCCESS;
}
