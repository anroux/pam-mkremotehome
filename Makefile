CURL_INC =
CURL_LIB = -lcurl

JSONC_INC = $(shell pkg-config --cflags json-c)
JSONC_LIB = $(shell pkg-config --libs json-c)

INCS = $(CURL_INC) $(JSONC_INC)
LIBS = $(CURL_LIB) $(JSONC_LIB)

CFLAGS=-Wall -fPIC -DPIC -rdynamic -pedantic -fno-stack-protector $(INCS)

LIBDIR=/lib
ifeq ($(shell if uname -o | grep -q "GNU/Linux" ; then echo true; else echo false; fi),true)
    ifeq ($(shell if [ -e /etc/debian_version ] ; then echo true; else echo false; fi),true)
	DEB_HOST_MULTIARCH ?= $(shell dpkg -L libc6 | sed -nr 's|^/etc/ld\.so\.conf\.d/(.*)\.conf$$|\1|p')
	ifneq ($(DEB_HOST_MULTIARCH),)
	    LIBDIR=/lib/$(DEB_HOST_MULTIARCH)
	endif
    else ifeq ($(shell uname -m),x86_64)  # redhat?
	LIBDIR=/lib64
    endif
endif

PAM_DIR=$(LIBDIR)/security

all: pam_mkremotehome.so

pam_mkremotehome.so: pam.o
	$(CC) -shared $^ -o $@ -lpam -lpam_misc -ldl $(LIBS) 

install: pam_mkremotehome.so
	install -d $(DESTDIR)$(PAM_DIR)
	install -m 644 $< $(DESTDIR)$(PAM_DIR)

clean:
	rm -f *.o *.so *.so.*
